﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ex2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static Dictionary<string, string> fruitcolors()
        {
            var dict = new Dictionary<string, string>();
            dict.Add("banana", "yellow");
            dict.Add("tomato", "red");
            dict.Add("apple", "green");

            return dict;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = checkfruit();

        }
        public string checkfruit()
        {
            var dict = fruitcolors();
            var result = "";

            if (dict.ContainsKey(textBox1.Text))
            {
                result = "Yes! the fruit is on the list";
            }
            else
            {
                result = "Nope! the fruit is not on the list";
            }

            return result;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
